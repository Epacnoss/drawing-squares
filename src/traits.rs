pub trait Coloured {
    fn get_colour(&self) -> [f32; 4];
}
